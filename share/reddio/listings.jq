def b(v): if (v) then "1" else "" end;

def msg_vars:
	# Booleans
	" new=" + b(.new),
	" was_comment=" + b(.was_comment),

	# Numbers
	" created=\(.created_utc // "")",

	# Strings
	" author=" + (.author // "" | @sh),
	" dest=" + (.dest // "" | @sh),
	" distinguished=" + (.distinguished // "" | @sh),
	" first_msg_id=" + .first_message_name // "",
	" id=" + .name // "",
	" parent_id=" + .parent_id // "",
	" subject=" + (.subject // "" | @sh),
	" subreddit=" + (.subreddit // "" | @sh),
	" text=" + (.body | @sh),
	"\n";

if type == "array" then
	.[].data.children[]
elif type == "object" then
	if .kind == "Listing" then
		.data.children[]
	elif (.json | type) == "object" then
		.json.data.things[]
	else
		.
	end
else
	error("the data does not seem to be a listing")
end |

"kind=" + .kind,

# Comments
if .kind == "t1" then .data |
	if .likes == true then " voted=up up=1 down="
		elif .likes == false then " voted=down up= down=1"
		else " voted= up= down=" end,

	# Booleans
	" archived=" + b(.archived),
	" is_submitter=" + b(.is_submitter),
	" locked=" + b(.locked),
	" new=" + b(.new),
	" saved=" + b(.saved),
	" stickied=" + b(.stickied),
	if .score_hidden then " show_score= hide_score=1"
		else " show_score=1 hide_score=" end,

	# Numbers
	if .controversiality == 0 then " controversiality="
	else " controversiality=\(.controversiality // "")" end,
	" created=\(.created_utc // "")",
	" depth=\(.depth // "0")",
	" downs=\(.downs // "")",
	" edited=\(.edited // "")",
	" score=\(.score // "")",
	" ups=\(.ups // "")",
	if .gilded == 0 then " gilded="
		else " gilded=\(.gilded // "")" end,

	# Strings
	" author=" + (.author // "" | @sh),
	" context=" + (.context // "" | @sh),
	" distinguished=" + (.distinguished // "" | @sh),
	" id=" + .name // "",
	" link_id=" + .link_id // "",
	" link_title=" + (.link_title // "" | @sh),
	" parent_id=" + .parent_id // "",
	" subreddit=" + (.subreddit // "" | @sh),
	" text=" + (.body // "" | @sh),
	"\n"

# User
elif .kind == "t2" then .data |
	# Booleans
	" subscribed=" + b(.has_subscribed),
	" is_employee=" + b(.is_employee),
	" is_friend=" + b(.is_friend),
	" is_gold=" + b(.is_gold),
	" is_mod=" + b(.is_mod),
	" verified=" + b(.verified),
	" verified_email=" + b(.has_verified_email),

	# Numbers
	" comment_karma=\(.comment_karma // "")",
	" created=\(.created_utc // "")",
	" link_karma=\(.link_karma // "")",

	# Strings
	" id=t2_" + .id // "",
	" name=" + (.name // "" | @sh),
	"\n"

# Link/Submission
elif .kind == "t3" then .data |
	if .likes == true then " voted=up up=1 down="
		elif .likes == false then " voted=down up= down=1"
		else " voted= up= down=" end,

	# Booleans
	" archived=" + b(.archived),
	" clicked=" + b(.clicked),
	" hidden=" + b(.hidden),
	" is_meta=" + b(.is_meta),
	" is_self=" + b(.is_self),
	" locked=" + b(.locked),
	" over18=" + b(.over_18),
	" pinned=" + b(.pinned),
	" quarantine=" + b(.quarantine),
	" saved=" + b(.saved),
	" spoiler=" + b(.spoiler),
	" stickied=" + b(.stickied),
	" visited=" + b(.visited),
	" is_original_content=" + b(.is_original_content),
	" is_gallery=" + b(.is_gallery),
	if .hide_score then " show_score= hide_score=1"
		else " show_score=1 hide_score=" end,
	if .is_video then " is_video=1" else " is_video=" end,
	if .is_self or .is_gallery or .is_video then
		" is_link="
	else
		" is_link=1"
	end,

	# Numbers
	" created=\(.created_utc // "")",
	" downs=\(.downs // "")",
	" edited=\(.edited // "")",
	" num_comments=\(.num_comments // "")",
	" num_crossposts=\(.num_crossposts // "")",
	" score=\(.score // "")",
	" ups=\(.ups // "")",
	" upvote_ratio=\(.upvote_ratio // "")",
	if .gilded == 0 then " gilded="
		else " gilded=\(.gilded // "")" end,

	# Strings
	" author=" + (.author // "" | @sh),
	" distinguished=" + (.distinguished // "" | @sh),
	" domain=" + (.domain // "" | @sh),
	" id=" + .name // "",
	" subreddit=" + (.subreddit // "" | @sh),
	" text=" + (.selftext // "" | @sh),
	" title=" + (.title // "" | @sh),
	if .is_video then
		" url=" + (.url_overridden_by_dest // "" | @sh)
	else
		" url=" + (.url // "" | @sh)
	end,
	if .is_gallery then
		" url=" + (.url_overridden_by_dest // "" | @sh),
		" gallery=",
		([.media_metadata as $ids | .gallery_data.items[] |
			if ((.outbound_url // "") != "") then
				"* \(.outbound_url)\r"
			else "" end +

			"* \($ids[.media_id].s.u // "")" +

			if ((.caption // "") != "") then
				"\r* \(.caption)"
			else "" end
		] | join("\r\r") | @sh)
	else
		" gallery="
	end,
	if .crosspost_parent then
		" xpost_id=" + .crosspost_parent // "",
		(
			.crosspost_parent as $xpost_parent
			| .crosspost_parent_list[]
			| select(.name==$xpost_parent)
			| " xpost_sub=" + .subreddit_name_prefixed // ""
		)
	else
		""
	end,
	"\n"

# Message
elif .kind == "t4" then .data |
	" depth=0",
	msg_vars,

	if (.replies | type) == "object" then
		recurse(.replies.data.children[]?) | if .data then .data |
			"kind=t4",
			" depth=1",
			msg_vars
		else empty end
	else empty end

# Subreddit
elif .kind == "t5" then .data |
	# Booleans
	" over18=" + b(.over18),
	" quarantine=" + b(.quarantine),
	" spoilers_enabled=" + b(.spoilers_enabled),
	" favorited=" + b(.user_has_favorited),
	" banned=" + b(.user_is_banned),
	" contributor=" + b(.user_is_contributor),
	" moderator=" + b(.user_is_moderator),
	" muted=" + b(.user_is_muted),
	" subscribed=" + b(.user_is_subscriber),
	" wiki_enabled=" + b(.wiki_enabled),

	# Numbers
	" active_accounts=\(.accounts_active // "")",
	" active_users=\(.active_user_count // "")",
	" created=\(.created_utc // "")",
	" subscribers=\(.subscribers // "")",

	# Strings
	" id=" + .name // "",
	" name=" + (.display_name // "" | @sh),
	" title=" + (.title // "" | @sh),
	" type=" + .subreddit_type // "",
	" description=" + (.public_description // "" | @sh),
	" header_title=" + (.header_title // "" | @sh),
	" submit_text=" + (.submit_text // "" | @sh),
	" text=" + (.description // "" | @sh),
	"\n"

# More comments
elif .kind == "more" then .data |
	# Numbers
	" count=\(.count // "")",
	" depth=\(.depth // "0")",

	# Strings
	" id=" + .name // "",
	" parent_id=" + .parent_id // "",
	" children=" + (.children | join(",") // ""),
	"\n"

# Unknown
else error("unknown kind") end
